package br.com.chucknoris

import android.app.Application
import br.com.chucknoris.repository.FactRepository
import br.com.chucknoris.repository.FactRepositoryInterface
import br.com.chucknoris.ui.MainActivityViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App: Application() {

    val applicationModule = module(override = true) {
        viewModel {
            MainActivityViewModel(get())
        }
        single<FactRepositoryInterface> {
            FactRepository()
        }
    }

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(applicationModule)
        }
    }
}