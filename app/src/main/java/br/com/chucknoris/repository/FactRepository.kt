package br.com.chucknoris.repository

import br.com.chucknoris.ui.Fact
import io.reactivex.Observable
import java.lang.Exception

class FactRepository: FactRepositoryInterface {

    override fun searchFact(search: String): Observable<List<Fact>> {
        return Observable.create<List<Fact>> { emitter ->
            val listFacts = listOf(
                Fact(
                    categories = listOf(),
                    iconUrl = "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    id = "7ARSAkwQSQqsSHmBJbxEJw",
                    url = "https://api.chucknorris.io/jokes/7ARSAkwQSQqsSHmBJbxEJw",
                    value = "Chuck Norris won the tour de france... on an exercise bike."
                ),
                Fact(
                    categories = listOf(),
                    iconUrl = "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    id = "7ARSAkwQSQqsSHmBJbxEJw",
                    url = "https://api.chucknorris.io/jokes/7ARSAkwQSQqsSHmBJbxEJw",
                    value = "Chuck Norris won the tour de france... on an exercise bike."
                ),
                Fact(
                    categories = listOf(),
                    iconUrl = "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    id = "7ARSAkwQSQqsSHmBJbxEJw",
                    url = "https://api.chucknorris.io/jokes/7ARSAkwQSQqsSHmBJbxEJw",
                    value = "Chuck Norris won the tour de france... on an exercise bike."
                ),
                Fact(
                    categories = listOf(),
                    iconUrl = "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    id = "7ARSAkwQSQqsSHmBJbxEJw",
                    url = "https://api.chucknorris.io/jokes/7ARSAkwQSQqsSHmBJbxEJw",
                    value = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec aliquam purus et urna blandit, sit amet iaculis quam eleifend. Fusce in posuere sapien. Nunc sodales vehicula ligula. Donec condimentum purus nec vestibulum sollicitudin. Duis in ultrices erat. Nullam elit velit, luctus ac lectus vel, tempus vehicula magna. Mauris condimentum convallis tortor non fermentum. Suspendisse at velit at metus sagittis viverra sed eget nisi. Proin mauris justo, semper sit amet nunc et, pulvinar commodo purus. Mauris ultricies non nisi ut pellentesque.."
                ),
                Fact(
                    categories = listOf(),
                    iconUrl = "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    id = "7ARSAkwQSQqsSHmBJbxEJw",
                    url = "https://api.chucknorris.io/jokes/7ARSAkwQSQqsSHmBJbxEJw",
                    value = "Chuck Norris won the tour de france... on an exercise bike."
                ),
                Fact(
                    categories = listOf(),
                    iconUrl = "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    id = "7ARSAkwQSQqsSHmBJbxEJw",
                    url = "https://api.chucknorris.io/jokes/7ARSAkwQSQqsSHmBJbxEJw",
                    value = "Chuck Norris won the tour de france... on an exercise bike."
                ),
                Fact(
                    categories = listOf(),
                    iconUrl = "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    id = "7ARSAkwQSQqsSHmBJbxEJw",
                    url = "https://api.chucknorris.io/jokes/7ARSAkwQSQqsSHmBJbxEJw",
                    value = "Chuck Norris won the tour de france... on an exercise bike."
                ),
                Fact(
                    categories = listOf(),
                    iconUrl = "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    id = "7ARSAkwQSQqsSHmBJbxEJw",
                    url = "https://api.chucknorris.io/jokes/7ARSAkwQSQqsSHmBJbxEJw",
                    value = "Chuck Norris won the tour de france... on an exercise bike."
                ),
                Fact(
                    categories = listOf(),
                    iconUrl = "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    id = "7ARSAkwQSQqsSHmBJbxEJw",
                    url = "https://api.chucknorris.io/jokes/7ARSAkwQSQqsSHmBJbxEJw",
                    value = "Chuck Norris won the tour de france... on an exercise bike."
                ),
                Fact(
                    categories = listOf(),
                    iconUrl = "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    id = "7ARSAkwQSQqsSHmBJbxEJw",
                    url = "https://api.chucknorris.io/jokes/7ARSAkwQSQqsSHmBJbxEJw",
                    value = "Chuck Norris won the tour de france... on an exercise bike."
                ),
                Fact(
                    categories = listOf(),
                    iconUrl = "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    id = "7ARSAkwQSQqsSHmBJbxEJw",
                    url = "https://api.chucknorris.io/jokes/7ARSAkwQSQqsSHmBJbxEJw",
                    value = "Chuck Norris won the tour de france... on an exercise bike."
                ),
                Fact(
                    categories = listOf(),
                    iconUrl = "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    id = "7ARSAkwQSQqsSHmBJbxEJw",
                    url = "https://api.chucknorris.io/jokes/7ARSAkwQSQqsSHmBJbxEJw",
                    value = "Chuck Norris won the tour de france... on an exercise bike."
                ),
                Fact(
                    categories = listOf(),
                    iconUrl = "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    id = "vbNN-g_uT5moT9pHYMPnQQ",
                    url = "https://api.chucknorris.io/jokes/vbNN-g_uT5moT9pHYMPnQQ",
                    value = "Chuck Norris can literally charm pants off ladies. This of course is useful for his ninesomes."
                )
            )

            emitter.onError(Exception("Teste"))

            emitter.onNext(listFacts)
            emitter.onComplete()
        }
    }
}