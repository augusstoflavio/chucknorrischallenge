package br.com.chucknoris.repository

import br.com.chucknoris.ui.Fact
import io.reactivex.Observable
import java.util.*

interface FactRepositoryInterface {

    fun searchFact(search: String): Observable<List<Fact>>
}