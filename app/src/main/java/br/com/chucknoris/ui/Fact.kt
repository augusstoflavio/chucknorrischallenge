package br.com.chucknoris.ui

data class Fact (
    var categories: List<String>,
    var iconUrl: String,
    var id: String,
    var url: String,
    var value: String
)