package br.com.chucknoris.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.chucknoris.R

class FactAdapter(): RecyclerView.Adapter<FactAdapter.FactHolder>() {

    val LIMIT_BIG_TEXT = 80

    var facts: List<Fact> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FactHolder {
        val v = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.adapter_fact, parent, false)
        return FactHolder(v)
    }

    fun update(facts: List<Fact>) {
        this.facts = facts
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = facts.size

    override fun onBindViewHolder(holder: FactHolder, position: Int) {
        val fact = facts.get(position)

        holder.text.text = fact.value
        if (fact.value.length > LIMIT_BIG_TEXT) {
            holder.text.textSize = 22.0F
        } else {
            holder.text.textSize = 32.0F
        }

        // TODO preencher a categoria
        holder.category.text = "Categoria"
        // TODO colocar o botão de compartilhar para funcionario
    }

    inner class FactHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var category: TextView = itemView.findViewById(R.id.category)
        var text: TextView = itemView.findViewById(R.id.text_fact)
        var buttonShared: ImageView = itemView.findViewById(R.id.button_shared)
    }
}