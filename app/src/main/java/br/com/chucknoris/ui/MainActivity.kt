package br.com.chucknoris.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.chucknoris.R
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    val viewModel: MainActivityViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val factAdapter = FactAdapter()
        viewModel.getFacts().observe(this, Observer {
            factAdapter.update(it)
        })
        recycler_view_facts.adapter = factAdapter
        recycler_view_facts.layoutManager = LinearLayoutManager(this)

        viewModel.getLoading().observe(this, Observer {
            loading.setVisibility(if (it) View.VISIBLE else View.GONE)
        })

        viewModel.getLoading().observe(this, Observer {
            loading.setVisibility(if (it) View.VISIBLE else View.GONE)
        })

        viewModel.getError().observe(this, Observer {
            if (it != null) {
                Snackbar.make(frame_layout,it, Snackbar.LENGTH_SHORT)
                    .setAction("Tente novamente") {
                        Log.i("Main activity", "Tente denovo")
                    }.show()
            }
        })

        viewModel.loadFacts("Aqui")
    }
}
