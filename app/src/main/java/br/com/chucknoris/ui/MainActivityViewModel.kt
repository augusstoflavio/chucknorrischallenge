package br.com.chucknoris.ui

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.chucknoris.repository.FactRepositoryInterface
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class MainActivityViewModel(
    val factRepository: FactRepositoryInterface
): ViewModel() {

    private val facts: MutableLiveData<List<Fact>> = MutableLiveData()
    private val loading = MutableLiveData<Boolean>()
    private val error = MutableLiveData<String?>()

    init {
        error.value = null
        loading.value = false
        facts.value = listOf()
    }

    fun getFacts(): LiveData<List<Fact>> {
        return facts
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }

    fun getError(): LiveData<String?> {
        return error
    }

    @SuppressLint("CheckResult")
    fun loadFacts(search: String) {
        error.value = null
        loading.value = true

        factRepository.searchFact("asdasd")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorReturn {
                error.value = it.message
                return@onErrorReturn listOf<Fact>()
            }
            .subscribe {
                loading.value = false
                facts.value = it
            }
    }
}